#!/usr/bin/env perl
use Cwd;
my $localcwd = cwd;
opendir $dir, "$localcwd" or die "Cannot open directory!";
@files = readdir $dir;
closedir $dir;
print "LISTING OF DIRECTORY $localcwd\n";
print "FILE		TYPE\n";
for my $i (@files)
{
	print "\U$i";
	if (-d "$i")
	{
		print "		DIRECTORY\n";
	}
	else
	{
		print "		FILE\n";
	}
}
