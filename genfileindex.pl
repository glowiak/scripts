#!/usr/bin/env perl
# Generate ftp-like file lists in html

if($ARGV[0] == 1)
{
	print "$0 - generate ftp-like file lists to html\n";
	print "Usage: $0 {directory with files} {html index}\n";
	exit(0);
}
else {
    $index_dir = $ARGV[0];
    $index_file = $ARGV[1];

    print "==\> Writing head of $index_file\n";
    system("cat > $index_file << \"EOF\"
<html>
<head>
<title>Index of $index_dir</title>
</head>
<body>
<p>Index of $index_dir
<br>
EOF");
    opendir my $dir, "$index_dir" or die "Cannot open directory $index_dir!";
    my @flist = readdir $dir;
    closedir $dir;
    for my $i (@flist)
    {   
    	print "==\> Adding file: $i\n";
    	system("echo \"<a href=$i>$i</a>\" \>\> $index_file");
    	system("echo \"<br>\" \>\> $index_file");
    }
    print "==\> Writing ending of $index_file\n";
    system("cat >> $index_file << \"EOF\"
</p>
</body>
</html>
EOF");
    print "==\> Done writing index of files to $index_file\n";
    exit(0);
}
